from django.urls import path,include
from rest_framework import routers
from api import views

from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('bill', views.BillViewSet, basename='bill')

app_name = 'api'

urlpatterns = [
    path('user/', views.CreateUserView.as_view(), name='create'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('me/', views.ManageUserView.as_view(), name='me'),
    path('balance/', views.BalanceAPIView.as_view(), name='balance'),
    path('', include(router.urls))
]