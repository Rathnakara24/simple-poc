from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a new user"""
        if not email:
            raise ValueError("User must have an email address")
    
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)  # self._db will be usefull when using multiple db

        return user

    def create_superuser(self, email, password):
        """Creates and saves new super user"""
        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that supports using email instead of username"""
    email = models.EmailField(max_length=254, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        """String Representation of the object"""
        return self.email


class Bill(models.Model):
    """Object representation of Bill Table. Id will be auto created by Django"""
    description = models.CharField(max_length=255, blank=False)
    amount = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """String representation of the object"""
        return self.description


class Balance(models.Model):
    """Object representation of the Bill table, this contains Bill and User information"""
    bill = models.ForeignKey(Bill,
    on_delete= models.CASCADE,
    related_name = 'balance'
    )
    bill_owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='bill_owner'
    )
    bill_payee = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='bill_payee'
    )
    amount = models.FloatField(default=0)   

