from rest_framework import serializers
from api import models
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _


class UserSerializer(serializers.ModelSerializer):
    """Serializer class for User table"""
    class Meta:
        model  = models.User
        fields = ('id', 'name', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True, 'style': {'input_type': 'password'}}}
    
    def create(self, validated_data):
        """Create a new user with encrypted password and return"""
        return get_user_model().objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()
        return user


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication object"""
    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password
        )
        if not user:
            msg = _("Unable to authenticate with provided credentials")
            raise serializers.ValidationError(msg, code='authentication')

        attrs['user'] = user
        return attrs


class BalanceSerializer(serializers.ModelSerializer):
    """Serializer class for Balance Model"""

    class Meta:
        model = models.Balance
        fields = ('id', 'amount', 'bill', 'bill_owner', 'bill_payee')
        read_only_fields = ('bill_owner', 'bill')



class BillSerializer(serializers.ModelSerializer):
    """Serializer for Bill"""
    balance = BalanceSerializer(many=True)

    class Meta:
        model = models.Bill
        fields = ('id', 'description','amount', 'created_at', 'balance')    