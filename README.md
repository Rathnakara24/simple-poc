Navigate to the directory and RUN 

sudo docker-compose up --build

***Endpoints***


***Creating User and seeing user details***

POST http://localhost:8085/api/user/

     body = { 
         "email":"sample@gmail.com", 
         "name":"Sample", 
         "password":"sample_password"
         }

GET http://localhost:8085/api/me/ 

    header = {
        "Authorization": "Token <token_value>"}
        
    , Responds with Logged in user detail


***Creating Token for the user***

POST http://localhost:8085/api/token/ 

    body = {
        "email": "sample@gmail.com",
        "password":"sample_password"
    } 
    response will contain a token

Use the above toke in Rest of the end points or else Authentication error will be raised


***Creating a Bill***

POST http://localhost:8085/api/bill/ 

    header = {"Authorization": "Token <token_value>}"
    
    body = {
    "description": "Sample",
    "amount": 500,
    "balance": [
        {"bill_payee":3, "amount":100},
        {"bill_payee":4, "amount":300},
        {"bill_payee":5, "amount":100}
        ]
}

Response = Created object


*** Checking Balance ***

GET http://localhost:8085/api/balance/

    header = {"Authorization": "Token <token_value>}"


*** List of all bills ***

GET http://localhost:8085/api/bill/ 

    header = {"Authorization": "Token <token_value>}"




***There are 3 users created in the System***

User A => Name: User A, email : usera@gmail.com , password: password123, userid = 3

User B => Name: User B, email : userb@gmail.com , password: password123, userid = 4

User C => Name: User B, email : userc@gmail.com , password: password123, userid = 5


Login with User A, and call http://localhost:8085/api/balance/ and http://localhost:8085/api/bill/