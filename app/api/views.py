from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework import generics, authentication, permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

from api.serializers import UserSerializer, AuthTokenSerializer, BalanceSerializer, BillSerializer

from api import models
from rest_framework import serializers


class CreateUserView(generics.CreateAPIView):
    """Creates a new user in the system"""
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""
    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authenticated user"""
        return self.request.user



class BalanceAPIView(generics.ListAPIView):
    """Manage the Balances of user"""
    serializer_class = BalanceSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = models.Balance.objects.all()

    def get_queryset(self):
        """Retrieve and return authenticated user"""
        return self.queryset.filter(Q(bill_payee=self.request.user))


class BillViewSet(viewsets.ModelViewSet):
    """Manage Balance in the database"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Bill.objects.all()
    serializer_class = BillSerializer

    def get_queryset(self):
        """Retrieve and return authenticated user"""
        return self.queryset.filter(Q(balance__bill_owner=self.request.user) | Q(balance__bill_payee=self.request.user)).distinct()


    def perform_create(self, serializer):
        """Saves the Bill object with the authenticated user"""
        balances = serializer.data['balance']
        amount = serializer.data['amount']
        
        sum = 0
        for balance_dict in balances:
            sum = sum+ balance_dict['amount']
        
        if sum != amount:
            msg = "Invalid amount"
            raise serializers.ValidationError(msg)
        
        #Create bill

        bill = models.Bill()
        bill.amount = serializer.data['amount']
        bill.description = serializer.data['description']
        bill.save()

        for balance_dict in balances:
            try:
                balance = models.Balance()
                balance.amount = balance_dict['amount']
                balance.bill = bill
                balance.bill_owner = self.request.user
                balance.bill_payee = models.User.objects.get(id=balance_dict['bill_payee'])
                balance.amount = float(balance_dict['amount'])
                balance.save()
            except models.User.DoesNotExist:
                raise serializers.ValidationError("Specified user does not exist")
        
        return Response(serializer.data, status=status.HTTP_201_CREATED)